var express = require('express');
var router = express.Router();
var request = require('request');
var http = require('http');
var peopleModel = require('../models/people.js');

var printConsole = peopleModel.stuff();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'Skills Matrix',
    definitionURL: printConsole,
    author: 'dufus'
  });

});

module.exports = router;
