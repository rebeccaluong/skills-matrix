console.log('d3');

  var dataTasks = [
    {
      Name: "Ainsley",
      Tasks: []
    }, {
      Name: "Ali",
      Tasks: ["Build - Campaign Canvas", "Build - Lead Scoring", "Build - Segment", "Build - External Call", "Build - CDO"]
    }, {
      Name: "Andrew",
      Tasks: [
        "Build - Analyzer Report",
        "Build - Insight Report",
        "Build - Dynamic Content",
        "Build - CDO",
        "Build - Lead Scoring",
        "Build - Campaign Canvas",
        "Build - Program Builder",
        "Consulting - Email (Certified)"
      ]
    }, {
      Name: "Brandi",
      Tasks: [
        "Build - Dynamic Content",
        "Build - CDO",
        "Build - Segment",
        "Build - Lead Scoring",
        "Build - Campaign Canvas",
        "Build - Program Builder"
      ]
    }, {
      Name: "Helen",
      Tasks: [
        "Build - Dynamic Content",
        "Build - CDO",
        "Build - External Call",
        "Build - Segment",
        "Build - Lead Scoring",
        "Build - Campaign Canvas",
        "Consutling - Validation Web Tracking Scripts",
        "Consulting - Email (Certified)"
      ]
    }, {
      Name: "Jesse",
      Tasks: [
        "Build - API",
        "Build - Dynamic Content",
        "Build - CDO",
        "Build - External Call",
        "Build - CRM",
        "Build - Subscription Management (Custom)",
        "Build - Segment",
        "Build - Lead Scoring",
        "Build - Campaign Canvas",
        "Build - Program Canvas",
        "Build - Program Builder",
        "Build - Form (Gated - Back/Front)",
        "Build - Form (Back/Front)",
        "Build - Landing Page",
        "Consutling - Validation Web Tracking Scripts",
        "Consulting - Branding & Deliverability - Standard+",
        "Consulting - Email (Certified)"
      ]
    }, {
      Name: "JessicaJones",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "JessicaV",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Sheroy",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Mike",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Mythili",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Rebecca",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Patrick",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Sarah",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Tim",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Tyler",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }, {
      Name: "Shannon",
      Tasks: ["Build - Campaign Canvas", "Build - Program Builder", "Consutling - Validation Web Tracking Scripts", "Consulting - Email (Certified)"]
    }
  ];
  var dataset = [
    {
      "id": "Ainsley",
      "Tasks": 0,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B72a231da-b3bc-4ea0-b251-1765e7a823ff%7D_Ainsley.png"
    }, {
      "id": "Ali",
      "Tasks": 6,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B6f9edb99-8990-4694-8b9d-5995799f0fae%7D_Ali.png"
    }, {
      "id": "Andrew",
      "Tasks": 7,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B3f1bdb2b-a778-4a95-b9e9-5942196a63b1%7D_Andrew.png"
    }, {
      "id": "Brandi",
      "Tasks": 6,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B24671a45-3286-4ea5-b9da-0620c1199a92%7D_Brandi.png"
    }, {
      "id": "Helen",
      "Tasks": 7,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B7d4c7241-f69a-4216-a1c0-0ab52a8e0750%7D_Hlau.jpg"
    }, {
      "id": "Jesse",
      "Tasks": 16,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7Bb0346794-a117-4a7c-900e-9446997d5619%7D_Jesse.png"
    }, {
      "id": "JessicaJones",
      "Tasks": 3,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B4d84e2f8-f5ff-4112-9151-56ddc75fa6d3%7D_Jessica-J.png"
    }, {
      "id": "JessicaV",
      "Tasks": 0,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7Baba374e1-28d5-4e13-a90c-c50baf13817a%7D_Jessica-V.png"
    }, {
      "id": "Mike",
      "Tasks": 4,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B051fa2b2-db92-4963-8595-e15750ccce13%7D_Mike.png"
    }, {
      "id": "Mythili",
      "Tasks": 4,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B3ff4ecb3-e7a1-4991-ac31-c6c505bdc46b%7D_Mythili.png"
    }, {
      "id": "Patrick",
      "Tasks": 7,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7Be553c6ab-a078-4850-8eb5-1b13b03b7bd6%7D_Patrick.png"
    }, {
      "id": "Rebecca",
      "Tasks": 4,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B2f047975-e5ac-4ff6-9328-bd30d1bc60e7%7D_Rebecca.png"
    }, {
      "id": "Sarah",
      "Tasks": 0,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B7f022a8c-809d-402c-aa47-449fdaf689ee%7D_Sarah.png"
    }, {
      "id": "Shannon",
      "Tasks": 6,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B9cdae808-27f8-4294-be41-3eb6e07bf515%7D_Shannon.png"
    }, {
      "id": "Sheroy",
      "Tasks": 9,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B579cd2e2-435d-4821-a7f3-3f81111146cc%7D_Sheroy.png"
    }, {
      "id": "Tim",
      "Tasks": 10,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B1413c716-6ad2-486a-b7b2-429de1c2fd4b%7D_Tim.png"
    }, {
      "id": "Tyler",
      "Tasks": 2,
      "Image": "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B700c95df-f26e-4221-a15a-b70bab474520%7D_ninja-simple-512.png"
    }
  ];
  var width = 960,
    height = 600;
  var svg = d3.select('#chart').append("svg").attr("height", height).attr("width", width).append("g").attr("transform", "translate(0,0)")
  var defs = svg.append("defs");
  defs.append("pattern").attr("id", "jon-snow").attr("height", "100%").attr("width", "100%").attr("patternContentUnits", "objectBoundingBox").append("image").attr("height", 1).attr("width", 1).attr("preserveAspectRatio", "none").attr("xmlns:xlink", "http://www.w3.org/1999/xlink").attr("xlink:href", "http://img04.en25.com/EloquaImages/clients/Tegrita/%7B72a231da-b3bc-4ea0-b251-1765e7a823ff%7D_Ainsley.png")
  var radiusScale = d3.scaleSqrt().domain([0, 16]).range([10, 80])
  var simulation = d3.forceSimulation().force("x", d3.forceX(width / 2).strength(0.5)).force("y", d3.forceY(height / 2).strength(0.5)).force("collide", d3.forceCollide(function(d) {
    return radiusScale(d.Tasks) + 2;
  }))
  d3.queue().await(ready)
  function ready(error, datapoints) {
    defs.selectAll(".team-pattern").data(dataset).enter().append("pattern").attr("class", "team-pattern").attr("id", function(d) {
      return d.id.toLowerCase().replace(/ /g, "-")
    }).attr("height", "100%").attr("width", "100%").attr("patternContentUnits", "objectBoundingBox").append("image").attr("height", 1).attr("width", 1).attr("preserveAspectRatio", "none").attr("xmlns:xlink", "http://www.w3.org/1999/xlink").attr("xlink:href", function(d) {
      return d.Image
    })
    var circles = svg.selectAll(".teamMembers").data(dataset).enter().append("circle").attr("class", "teamMembers").attr("data-layer-target", function(d) {
      return "#" + d.id
    }).attr("data-id", function(d) {
      return d.id
    }).attr("r", function(d) {
      return radiusScale(d.Tasks)
    }).attr("fill", function(d) {
      return "url(#" + d.id.toLowerCase().replace(/ /g, "-") + ")"
    }).attr("cx", 100).attr("cy", 300)
    simulation.nodes(dataset).on('tick', ticked)
    function ticked() {
      circles.attr("cx", function(d) {
        return d.x
      }).attr("cy", function(d) {
        return d.y
      })
    }
    //puts circles in the forefront on hover
    d3.selection.prototype.moveToFront = function() {
      return this.each(function() {
        this.parentNode.appendChild(this);
      });
    };
    circles.on("mouseover", function() {
      var sel = d3.select(this);
      sel.moveToFront();
    });
  }
  //on click add modal and finds team member's name
  $('.teamMembers').on('click', function(evt) {
    $('#teamTasks').empty();
    var cirTeamMem = (evt.target.correspondingUseElement)
      ? evt.target.correspondingUseElement
      : evt.target;
    var teamMemName = cirTeamMem.getAttribute("data-id");
    const tTask = dataTasks.filter(function(teamMember) {
      if (teamMember.Name == teamMemName) {
        return true;
      }
    });
    $(".modal").attr("id", function(evt) {
      return teamMemName;
      //adds id attribute of selected circle teamMember
    });
    //adds team name to modal
    $('#teamName').html(teamMemName);
    //adds list of tasks to modal
    for (var i in tTask[0].Tasks) {
      $("#teamTasks").append("<li>" + tTask[0].Tasks[i] + " " + "</li>");
      $("#teamTasks").append("</li>");
    }
  });
  console.log("end");
