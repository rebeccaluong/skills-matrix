// Include the request library for Node.js
var request = require('request');
var http = require('http');

//  Basic Authentication credentials
var username = 'Tegrita\\Rebecca.Luong';
var password = '8C6bxm#&*51X';
var authenticationHeader = 'Basic ' + new Buffer(username + ':' + password).toString('base64');

function createDef() {
  data = {
    'name': 'Skills Matrix Export',
    'fields': {
      'Email_Address': '{{CustomObject[28].Field[460]}}',
      'Name': '{{CustomObject[28].Field[506]}}',
      'Sub_Task': '{{CustomObject[28].Field[458]}}',
      'Image_Link1': '{{CustomObject[28].Field[459]}}'
    }
  };
  request({
    url: 'https://secure.p04.eloqua.com/api/bulk/2.0/customObjects/28/exports',
    headers: {
      'Authorization': authenticationHeader
    },
    method: 'POST',
    json: data
  }, function(error, response, body) {
    var defURI = (body.uri);
    console.log(defURI);
    return defURI;
    // stageRequest(defURI);
  });
}
function printStuff() {
  console.log('stuff');
  x = 1 + 3;
  return x;
}

module.exports.stuff = printStuff;

//
// // step 2: here grab the URI to get all the data have to save uri into a json and pass through the /syncs  => this will get you the actual synced instance URI
// function stageRequest(defURI) {
//   var syncedInstanceUri = defURI;
//
//   syncedURI = {
//     'syncedInstanceUri': syncedInstanceUri
//   };
//   request({
//     url: 'https://secure.p04.eloqua.com/api/bulk/2.0/syncs',
//     headers: {
//       'Authorization': authenticationHeader
//     },
//     method: 'POST',
//     json: syncedURI
//   }, function(error, response, body) {
//     var stageURI = body.uri;
//     setTimeout(function() {
//       retrieveData(stageURI);
//     }, 1000);
//   });
// }
// // step 3: pass synced instance URI to pass onto another req call '/data', from here ccombine base URL concat with synced instance URI and /data i.e. (https://secure.p04.eloqua.com/api/bulk/2.0/customObjects/28/exports/syncs/6/data)=> this expires every hour maybe
// function retrieveData(stageURI) {
//   var baseURL = 'https://secure.p04.eloqua.com/api/bulk/2.0';
//   var syncedURI = stageURI;
//
//   request({
//     url: baseURL + syncedURI + '/data',
//     headers: {
//       'Authorization': authenticationHeader
//     },
//     method: 'GET',
//     json: true
//   }, function(error, response, body) {
//
//     var cdoData = body.items;
//     sumTasks(cdoData);
//     sumPeople(cdoData);
//     // console.log(cdoData);
//   });
// }
//
// //   function sumTasks(cdoData) {
// //
// // //takes in cdoData array
// //     var aNames = cdoData.reduce(function(a,b) {
// //       if (a.indexOf(b) === -1) {
// //         //b.Name is will check in a array to see if item exists
// //         a.push({Name:b.Name});
// //         //if item does nto exist, push {} into a[]
// //       } return a;
// //     }, []);
// //   }
//
// //how to push the whole key:value but also check it
//
// function sumTasks(cdoData) {
//
//   var aNames = cdoData.reduce(function(array, element) {
//     // The first condition makes sure the array (aNames) is not empty
//     // The second condition checks if the last element has the same name
//     // as the current element we are looping over
//
//     //if array is empty and if last element name is equal to the compared element name, push to task key of the element.
//
//     // The reason we check only the last element is because we are checking
//     // during each iteration. So we don't need to loop over the entire array
//     // every single time
//     if (array.length !== 0 && array[(array.length - 1)].name == element.Name) {
//       // If the name matches, we push the task to the task key of that array element
//       // instead of creating a new entry in the array
//       array[(array.length - 1)].task.push(element.Sub_Task);
//     } else {
//       // If it does not, we just add a new entry
//       // Notice how the value for task is an array, this is done because
//       // we need to push to it later
//       array.push({
//         name: element.Name,
//         task: [element.Sub_Task]
//       });
//     }
//     return array;
//   }, []);
//   // console.log(aNames);
//   return aNames;
// }
//
// function sumPeople(cdoData) {
//
//   var aPeople = cdoData.reduce(function(array, element) {
//
//     if (array.length !== 0 && array[(array.length - 1)].id == element.Name) {
//       // if array is not empty AND last item of array.id(name) is equal to the current name add 1 to task value
//
//       array[(array.length - 1)].tasks += 1;
//
//     } else {
//       // If it does not, we just add a new entry and start tasks count at one.
//       array.push({id: element.Name, tasks: 1, Image: element.Image_Link1});
//     }
//     return array;
//   }, []);
//   console.log(aPeople);
//   return aPeople;
// }
